<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 19:08
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';
    protected $fillable = ['title', 'description', 'image1', 'image2', 'image3'];

}