<?php

namespace App\Http\Controllers;

use Auth;
use App\Address;
use App\About;
use App\Product;
use App\Representative;
use Illuminate\Support\Facades\DB;

class BackEndController extends Controller{

    public function showProfile() {
        return view('backend.pages.profile', ['page_title' => 'Profile Settings']);
    }

    public function showContacts() {
        $address = Address::where('id', 1)->first();
        if($address!= null) {
            return view('backend.pages.contacts', ['page_title' => 'Contacts', 'address' => $address]);
        }else {
            return view('backend.pages.contacts', ['page_title' => 'Contacts', 'address' => null]);
        }
    }

    public function showAboutUs() {
        $about = About::where('id', 1)->first();
        if($about!= null) {
            return view('backend.pages.about-us', ['page_title' => 'About Us', 'about' => $about]);
        }else {
            return view('backend.pages.about-us', ['page_title' => 'About Us', 'about' => null]);
        }
    }

    public function showMap() {
        $map = DB::table('map')->where('id', 1)->first();
        return view('backend.pages.map', ['page_title' => 'Map Editor', 'map' => $map]);
    }

    public function savePosition() {
        /*DB::table('map')->where('id', 1)
            ->update(array('latitude' => $_REQUEST['lat'], 'longitude' => $_REQUEST['long']));*/
        DB::table('map')->where('id', 1)
            ->update(array('url' => $_REQUEST['link']));
    }
/*Products*/
    public function showAllProducts() {
        $products = Product::all();
        return view('backend.products.view', ['page_title' => 'All Products', 'products' => $products]);
    }

    public function createNewProduct() {
        return view('backend.products.create', ['page_title' => 'Create New Product']);
    }

    public function editProduct($id) {
        $product = Product::where('id', $id)->first();
        return view('backend.products.update', ['page_title' => 'Edit Product', 'product' => $product]);
    }

    public function removeProduct($id) {
        $product = Product::where('id', $id)->delete();
        return redirect('products');
    }

    public function showProduct($id) {
        $product = Product::where('id', $id)->first();
        return view('backend.products.index', ['page_title' => $product->title, 'product' => $product]);
    }
/*Companies*/
    public function showAllCompanies() {
        $companies = Representative::all();
        return view('backend.companies.view', ['page_title' => 'All Companies', 'companies' => $companies]);
    }

    public function createNewCompany() {
        return view('backend.companies.create', ['page_title' => 'New Company Entry']);
    }

    public function editCompany($id) {
        $company = Representative::where('id', $id)->first();
        return view('backend.companies.update', ['page_title' => 'Edit Company', 'company' => $company]);
    }

    public function showCompany($id) {
        $company = Representative::where('id', $id)->first();
        return view('backend.companies.index', ['page_title' => 'Company View', 'company' => $company]);
    }

    public function removeCompany($id) {
        $company = Representative::where('id', $id)->delete();
        return redirect('companies');
    }
}