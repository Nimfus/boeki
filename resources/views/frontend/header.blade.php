<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>SP BOEKI</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href='{{asset('fonts/calibri_7.ttf')}}' rel='stylesheet' type='text/css'>
    <link href='{{asset('fonts/calibrib_5.ttf')}}' rel='stylesheet' type='text/css'>
    <link href='{{asset('fonts/calibril.ttf')}}' rel='stylesheet' type='text/css'>

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Scripts
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <style>
        html,
        body {
            overflow-x: hidden;
            background: #f5f5f5;
            font-family: Calibri;
            color: #696969;
            font-weight: 200;
            font-size: 16px;
        }
        a:hover,
        a:visited,
        a:link,
        a:active,
        a:focus {
            text-decoration: none;
        }
        h1 {
            text-align: center;
        }
        .container-fluid {
            padding: 0;
        }
        .logo-responsive {
            display: block;
            margin: 15% auto;
        }
        .navigation {
            background: white;
            min-height: 70px;
            z-index: 10;
            margin: 0 auto;
            width: 100%;
            padding: 0;
        }
        .logo-img {
            max-height: 70px;
        }
        .navbar-right {
            width: 100%;
            text-align: center;
            display: block;
        }
        .navbar-nav li {
            width: 10%;
            min-height: 70px;
            text-align: center;
        }
        .top-menu-item:hover {
            background: #f5f5f5;
            transition: 0.3s;
        }
        .navbar-nav li a {
            padding: 0;
            min-height: 30px;
            color: slategrey;
        }
        .navbar-nav li a:hover {
            background: inherit;
        }
        .page-link {
            margin-top: 24px;
        }
        .top-menu-logo {
            margin: 0 10% 0 10%;
        }
        .header {
            position: relative;
            width: 100%;
            height: 100vh;
            text-align: center;
            color: #DFDDDD;
            background-image: url(../img/header-img.jpg);
            background-position: center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-size: cover;
            -o-background-size: cover;
            margin: 0;
        }

        .header .header-content {

        }

        .header .header-content .header-content-inner h1 {
            margin-top: 0;
            margin-bottom: 0;
            text-transform: uppercase;
            font-weight: 700;
        }

        .header .header-content .header-content-inner hr {
            margin: 30px auto;
        }

        .header .header-content .header-content-inner p {
            margin-bottom: 50px;
            font-size: 16px;
            font-weight: 300;
            color: rgba(255,255,255,.7);
        }
        /* ---------------------------------------------*/
        /* About us */
        .about-us {
            margin-top: 1px;
            text-align: center;
            padding: 0 20% 0 20%;
        }
        .about-us ul {
            width: 60%;
            margin: auto;
            list-style: square outside;
            text-align: left;
        }
        .about-us h1 {
            margin: 60px 0 40px 0;
        }
        .about-us h4 {
            margin: 30px auto;
        }
        /* ---------------------------------------------*/

        /* Products */

        .representative{
            padding: 0 10% 0 10%;
        }
        .r-logo {
            background: green;
        }
        /* ---------------------------------------------*/

        /* Representatives */
        .r-logo img {
            height: 200px;
        }
        /* ---------------------------------------------*/

        /* Contact Us */
        #map {
            width: 80%;
            height: 350px;
        }
        .contact-left {
            padding-top: 50px;
        }
        /* ---------------------------------------------*/

        /* Feedback */
        .input-line {
            margin: 0 auto;
        }
        .enquiry {
            text-align: center;
            margin: auto;
            padding: 0 20% 0 20%;
        }
        .text-block {
            text-align: left;
        }
        .form-horizontal .control-label {
            padding-top: 0;
            padding-bottom: 8px;
        }
        .contact-us h1,
        .enquiry h1 {
            margin-top: 30px;
            margin-bottom: 40px;
        }
        .enquiry label {
            display: block;
            text-align: right;
            padding-right: 15px;
        }
        .enquiry input {
            display: block;
            width: 90%;
            background: #f5f5f5;
            border: 1px solid #d3d3d3;
        }
        .enquiry textarea {
            width: 90%;
            height: 100px;
            background: #f5f5f5;
            resize: none;
            border: 1px solid #d3d3d3;
        }
        .remarks {
            color: #808080;
        }
        .remarks span {
            font-size: 16px;
        }
        .remarks input {
            width: 80px;
            background: #3871a4;
            color: white;
        }
        .remarks input:hover {
            background: #2e5e8a;
            transition: 0.3s;
        }
        /* ---------------------------------------------*/
        .divider {
            width: 80%;
            border-color: #c1c1c1;
        }
        footer {
            height: 150px;
            background: #303030;
            margin-top: 50px;
        }
        i {
            color: #1e6ea4;
            font-size: 18px;
        }
    </style>
</head>
<body>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="container-fluid">
    <div class="navbar-collapse navbar-fixed-top navigation">
        <ul class="nav navbar-nav navbar-right">
            <li class="top-menu-logo">
                <a href="#home"><img class="logo-img" src="{{asset('img/logo.png')}}"></a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#home">Home</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#about">About Us</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#products">Products</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#contact">Contact</a>
            </li>
        </ul>
    </div>