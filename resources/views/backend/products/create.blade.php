@extends('backend.master')
<style>
    form {
        margin: 0 30% 0 10%;
    }
    #description {
        height: 300px;
        resize: none;
    }
    .cropit-image-preview {
        /* You can specify preview size in CSS */
        width: 200px;
        height: 200px;
    }
    .cropit-image-preview h4 {
        display:block;
    }

</style>
@section('content')
    <form method="POST" ><!--action="/products/store" enctype="multipart/form-data"-->
         {!! csrf_field() !!}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Product Title">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Product Description"></textarea>
        </div>
        <div class="form-group">
            <label for="image1">First Image <span style="font-weight: 200;">(Minimum required dimensions - 300px x 300px)</span></label>
            <div id="image-cropper1">
                <input type="file"  name="image1" id="image1"  class="cropit-image-input">
                <div class="cropit-image-preview"></div>
                <input type="range" class="cropit-image-zoom-input" />
                <input type="hidden" name="image-data1" class="hidden-image-data" />
            </div>
        </div>
        <div class="form-group">
            <label for="image2">Second Image <span style="font-weight: 200;">(Minimum required dimensions - 300px x 300px)</span></label>
            <div id="image-cropper2">
                <input type="file" name="image2" id="image2" class="cropit-image-input">
                <div class="cropit-image-preview"></div>
                <input type="range" class="cropit-image-zoom-input" />
                <input type="hidden" name="image-data2" class="hidden-image-data" />
            </div>
        </div>
        <div class="form-group">
            <label for="image3">Third Image <span style="font-weight: 200;">(Minimum required dimensions - 300px x 300px)</span></label>
            <div id="image-cropper3">
                <input type="file"  name="image3" id="image3" class="cropit-image-input">
                <div class="cropit-image-preview"></div>
                <input type="range" class="cropit-image-zoom-input" />
                <input type="hidden" name="image-data3" class="hidden-image-data" />
            </div>
        </div>
        <button  class="btn btn-default" id="saveProduct">Save</button>
    </form>
    <script>
        $(document).ready(function() {
           $(document).on('click', '#saveProduct', function(e) {
               e.preventDefault();
               $.ajax({
                   url: '/products/store',
                   type: "post",
                   data: {
                       '_token': $('#_token').val(),
                       'title': $('#title').val(),
                       'description': $('#description').val(),
                       'image1': $('#image-cropper1').cropit('export'),
                       'image2': $('#image-cropper2').cropit('export'),
                       'image3': $('#image-cropper3').cropit('export')
                   },
                   success: function(){
                       document.location.href = "/products"
                   }
               });
           });
        });
    </script>
@endsection
