<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 14:40
 */

namespace App\Http\Controllers;

use App\About;
use App\Http\Requests\AboutRequest;

class AboutController extends Controller {

    public function store(AboutRequest $request) {
        $about = About::where('id', 1)->first();

        if($about!=null){
            $about->contents = $request->contents;

            $about->save();

            return view('backend.pages.about-us', ['page_title' => 'About Us', 'about' => $about]);
        }else {
            $about = new About([
                'contents' => $request->contents,
            ]);

            $about->save();
            return view('backend.pages.about-us', ['page_title' => 'About Us', 'about' => $about]);
        }
    }

}