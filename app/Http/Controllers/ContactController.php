<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\AddressRequest;

class ContactController extends Controller {

    public function store(AddressRequest $request) {
        $address = Address::where('id', 1)->first();

        if($address!=null){
            $address->company_name = $request->company_name;
            $address->address_1 = $request->address_1;
            $address->address_2 = $request->address_2;
            $address->tel_1 = $request->tel_1;
            $address->tel_2 = $request->tel_2;
            $address->fax = $request->fax;
            $address->email = $request->email;

            $address->save();

            return view('backend.pages.contacts', ['page_title' => 'Contacts', 'address' => $address]);
        }else {
            $address = new Address([
                'company_name' => $request->company_name,
                'address_1' => $request->address_1,
                'address_2' => $request->address_2,
                'tel_1' => $request->tel_1,
                'tel_2' => $request->tel_2,
                'fax' => $request->fax,
                'email' => $request->email
            ]);

            $address->save();
            return view('backend.pages.contacts', ['page_title' => 'Contacts', 'address' => $address]);
        }
    }

}