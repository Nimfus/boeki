@extends('backend.master')
<style>
    .form-group {
        width: 80%;
    }
    .form-group input {
        width: 60%;
    }
    .remarks {
        text-align: center;
    }
    .input-area {
        height: 400px;
    }
</style>
@section('content')
    <form class="form-horizontal" action="/about-us/store" method="POST">
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="col-sm-3 control-label" for="company_name">Contents:</label>
            <div class="col-sm-9">

                <textarea class="input-area" name="contents" id="editor">@if($about!=null) {{$about->contents}} @endif</textarea>
            </div>
        </div>
        <div class="remarks">
            <div class="col-sm-12">
                <input class="submit-button btn btn-default" type="submit" value="Save">
            </div>
        </div>
    </form>
    <script>
        var editor = CKEDITOR.replace('editor', {
            extraPlugins: 'button,panelbutton,colorbutton,colordialog,uicolor,uploadimage,font,listblock,justify,liststyle,contextmenu,menu,floatpanel,panel,dialogui',
            height: 300,

            // Upload images to a CKFinder connector (note that the response type is set to JSON).
            uploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

            // Configure your file manager integration. This example uses CKFinder 3 for PHP.
            filebrowserBrowseUrl: '../js/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '../js/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

            // The following options are not necessary and are used here for presentation purposes only.
            // They configure the Styles drop-down list and widgets to use classes.

            stylesSet: [
                { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
                { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
            ],

            // Load the default contents.css file plus customizations for this sample.
            contentsCss: [ CKEDITOR.basePath + 'contents.css', 'http://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ],

            // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
            // resizer (because image size is controlled by widget styles or the image takes maximum
            // 100% of the editor width).
            image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
            image2_disableResizer: true
        });
    </script>
@endsection