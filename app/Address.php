<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 02:24
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Address extends Model {

    protected $table = 'about';
    protected $fillable = ['company_name', 'address_1', 'address_2', 'tel_1', 'tel_2', 'fax', 'email'];

}