<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active" id="about-us"><a href="{{asset('about-us')}}"><i class="fa fa-info-circle"></i><span>About Us</span></a></li>
            <li id="products"><a href="{{asset('products')}}"><i class="fa fa-cart-plus"></i> <span>Products</span></a></li>
            <li id="companies"><a href="{{asset('companies')}}"><i class="fa fa-share-alt"></i> <span>Companies</span></a></li>
            <li id="contact-us"><a href="{{asset('contact-us')}}"><i class="fa fa-users"></i> <span>Contacts</span></a></li>
            <li id="map"><a href="{{asset('map')}}"><i class="fa fa-map"></i> <span>Map</span></a></li>
            <li id="profile"><a href="{{asset('profile')}}"><i class="fa fa-user"></i> <span>Manage Account</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>