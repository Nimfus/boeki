@extends('backend.master')
<style>
    .btn-success,
    .table-striped {
        margin-left: 10%;
    }
    table a {
        margin-right: 20px;
    }
    .edit,
    .delete,
    .view {
        padding-left: 10px;
    }
    .edit:hover {
        color: purple;
    }
    .delete:hover {
        color: red;
    }
    .view:hover {
        color: green;
    }
    td a {
        text-align: center;
    }
    .sign-out-button {
        display: block;
        height: 24px;
        padding-top: 0;
        float: right;
        transition: 0.3s;
        position: relative;
        top: -20px;
    }
</style>
@section('content')
    <table class="table table-striped" style="width: 80%;">
        <thead><td>ID</td><td>Title</td><td>Link or Page</td><td>View</td><td>Edit</td><td>Remove</td></thead>
        <tbody>
        @foreach($companies as $company)
            <tr><td>{{$company->id}}</td><td>{{$company->title}}</td><td>@if($company->link!=null){{$company->link}}@else Internal Page @endif</td><td><a href="{{asset('companies/view/'.$company->id)}}"><i class="fa fa-eye view"></i></a></td><td><a href="{{asset('edit/'.$company->id)}}"><i class="fa fa-edit edit"></i></a></td><td><a href="{{asset('companies/remove/'.$company->id)}}"><i class="fa fa-trash-o delete"></i></a></td></tr>
        @endforeach
        </tbody>
    </table>
    <a class="btn btn-success" href="{{asset('new')}}">Create New</a>
@endsection