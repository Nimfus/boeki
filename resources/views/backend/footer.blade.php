<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <p><span>Created by</span>  <a href="https://businessbuddy.sg/"><img class="managed-by" src="{{asset('img/managed-by-logo.png')}}"></a></p>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2015 <a href="{{asset('/')}}">SP Boeki Pte Ltd</a>.</strong> All rights reserved.
</footer>