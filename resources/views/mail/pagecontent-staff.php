<?php $t =& peTheme(); ?>
<?php $content =& $t->content; ?>
<?php $meta =& $content->meta(); ?>
<?php $hasFeatImage = $content->hasFeatImage(); ?>
<?php $sbg = empty($meta->pagebg->transparent) ? $meta->pagebg->color : 'transparent'; ?>
<style>
.ma {
	margin: auto;
}
</style>
<section id="<?php $content->slug(); ?>" class="section section-staff" style="background-color:<?php echo $sbg; ?>">
	<div class="container">

		<div class="title col-md-8 col-sm-10 col-xs-12">
			<h1><?php $content->title(); ?></h1>
			<hr>
			<div class="section-content pe-wp-default"><?php $content->content(); ?></div>
		</div>

		<?php 

		if ( empty( $meta->staff->staff ) ) { 

			$staffs = get_posts( array( 'post_type' => 'staff', 'posts_per_page' => -1 ) );

			if ( is_array( $staffs ) ) {

				foreach( $staffs as $staff ) {

					$staffarray[] = $staff->ID;

				}

				$meta->staff->staff = $staffarray;

			}

		}

		?>

		<?php if ( ! empty( $meta->staff->staff ) ) : ?>

			<?php if ( $loop = $t->data->customLoop( (object) array( "post_type"=>"staff", "id" => $meta->staff->staff, "orderby" => "post__in", ) ) ) : ?>

				<?php $count = count( $meta->staff->staff ); ?>

				<?php $delay = 0; ?>
				
				<?php while ( $content->looping() ) : ?>

					<?php

						$meta =& $content->meta();
						$social = isset( $meta->info->social ) ? $meta->info->social : '';
						$position = isset( $meta->info->position ) ? $meta->info->position : '';
						$hasFeatImage = $content->hasFeatImage();

					?>

					<?php

						switch ( $count ) {

							case ( 1 ) :
								$class_first = 'col-lg-2 col-md-4 col-sm-6 col-xs-12 col-md-offset-4 col-sm-offset-3 ma';
								$class_others = 'col-lg-2 col-md-4 col-sm-6 col-xs-12 ma';
								break;

							case ( 2 ) :
								$class_first = 'col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-3 ma ';
								$class_others = 'col-lg-2 col-md-3 col-sm-6 col-xs-12 ma' ;
								break;

							case ( 3 ) :
								$class_first = 'col-lg-2 col-md-4 col-sm-4 col-xs-12 ma';
								$class_others = 'col-lg-2 col-md-4 col-sm-4 col-xs-12 ma';
								break;

							default :
								$class_first = 'col-lg-2 col-md-3 col-sm-6 col-xs-12 ma';
								$class_others = 'col-lg-2 col-md-3 col-sm-6 col-xs-12 ma' ;

						}

						$class = $delay === 0 ? $class_first : $class_others;

					?>

					<div class="<?php echo $class; ?>">
						<div class="team animated hiding" data-animation="fadeInUp" data-delay="<?php echo $delay; ?>">

							<?php if ( $hasFeatImage ) : ?>

								<div class="team-photo">
									
									<?php $content->img( 453, 576 ); ?>

									<div class="team-overlay">
										<div class="team-content"><?php $content->content(); ?></div>

										<?php if ( isset( $social ) && is_array( $social ) ): ?>

											<ul class="social-list">

												<?php foreach ( $social as $icon ) : ?>

													<?php 

														$icon['icon'] = explode( '|', $icon['icon'] );
														$icon['icon'] = $icon['icon'][0];

													?>

													<li>
														<a href="<?php echo $icon['url']; ?>" target="_blank">
															<span class="social-icon">
																<i class="<?php echo $icon['icon']; ?>"></i>
															</span>
														</a>
													</li>

												<?php endforeach; ?>
												
											</ul>

										<?php endif; ?>
									</div>

								</div>

							<?php endif; ?>

							<div class="team-inner" style="height: 120px; min-height: 120px; max-height: 120px;">
								<h4 style="text-align: center;"><?php $content->title(); ?></h4>
								<p><?php echo $position; ?></p>
							</div>

						</div>
					</div>

					<?php $delay += 500; ?>

				<?php endwhile; ?>

				<?php $content->resetLoop(); ?>

			<?php endif; ?>

		<?php endif; ?>

	</div>
</section>