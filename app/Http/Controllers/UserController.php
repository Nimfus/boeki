<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 02.12.15
 * Time: 13:20
 */

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;
use Appzcoder\LaravelRoles\Models\Role;
use App\Company;
use App\User;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Facades\Hash;

class UserController extends  Controller {

    public function changePassword(HasherContract $hasher) {
        $this->hasher = $hasher;
        $user = User::where('id', Auth::user()->id)->first();
        if(Hash::check($_REQUEST['password'], Auth::user()->password) && $_REQUEST['new_password'] == $_REQUEST['password_confirmation']) {
            $user->password = bcrypt($_REQUEST['new_password']);
            $user->save();
        }
    }

}