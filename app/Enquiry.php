<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 18.11.15
 * Time: 16:11
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model {

    protected $table = 'enquiries';
    protected $fillable = ['title', 'name', 'company', 'email', 'tel', 'enquiry_text', 'delivery_time'];
}