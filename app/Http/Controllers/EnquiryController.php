<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 18.11.15
 * Time: 16:13
 */

namespace App\Http\Controllers;

use App\Enquiry;
use App\Http\Requests\EnquiryRequest;
use Illuminate\Support\Facades\Mail;

class EnquiryController extends Controller {

    public function send(EnquiryRequest $request) {

        $enquiry = new Enquiry([
            'title' => $request->title,
            'name' => $request->name,
            'company' => $request->company,
            'email' => $request->email,
            'tel' => $request->tel,
            'enquiry_text' => $request->feedback,
        ]);

        $enquiry->save();
        if ($request->file('drawing') != null) {
            $logoName = $enquiry->id . '.' . $request->file('drawing')->getClientOriginalExtension();
            $enquiry->drawing = $enquiry->id . '.' . $request->file('drawing')->getClientOriginalExtension();
            $enquiry->save();
            $request->file('drawing')->move(base_path() . '/public/img/enquiries/', $logoName);
        }

        Mail::send('mail.master', [
            'title' => $_REQUEST['title'],
            'name' => $_REQUEST['name'],
            'company' => $_REQUEST['company'],
            'email' => $_REQUEST['email'],
            'tel' => $_REQUEST['tel'],
            'enquiry_text' => $_REQUEST['feedback'],
            'drawing' => $enquiry->drawing
        ], function($message) use ($enquiry){
            $message->from('support@example.com', 'Boeki')->to('nimfus@gmail.com', 'Administrator')->subject('Enquiry: '.$_REQUEST['title']);
            if ($enquiry->drawing!=null) {
                $message->attach(base_path() . '/public/img/enquiries/' . $enquiry->drawing);
            }
        });
    }
}