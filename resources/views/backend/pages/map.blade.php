@extends('backend.master')
<style>
#mapEdit {
    height: 50%;
}
    .control-label {
        padding-top: 6px;
    }
    .de {
        margin-bottom: 40px;
        margin-left: 30px;
    }
</style>
@section('content')
<div class="row de">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-1 control-label">Link</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="mapUrl" placeholder="Paste link to the map here">
            </div>
            <div class="col-sm-4">
                <button id="saveLongLat" class="submit-button btn btn-default">Save Link</button>
            </div>
        </div>
    </div>
</div>
{!!$map->url!!}
    <!--div id="mapEdit"></div>
    <p>New Latitude: <span id="Lat"></span></p>
    <p>New Longitude: <span id="Long"></span></p>
</div-->
<!--div class="col-sm-12">
    <button id="saveLongLat" class="submit-button btn btn-default">Save Position</button>
</div-->


<script>
    /*function initMap() { //Google maps 1.363291, 103.887685
        var myLatLng = {lat: parseFloat('{{$map->latitude}}'), lng: parseFloat('{{$map->longitude}}')};

        var map = new google.maps.Map(document.getElementById('mapEdit'), {
            zoom: 18,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Business Buddy Pte Ltd',
            icon: '{{asset('img/marker.png')}}'
        });
        google.maps.event.addListener(map, 'click', function(event) {
            marker.setMap(null);
            console.log(event.latLng.lat() + " " + event.latLng.lng());
            $('#Long').text(event.latLng.lng());
            $('#Lat').text(event.latLng.lat());

            myLatLng = {lat: event.latLng.lat(), lng: event.latLng.lng()}
            marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Business Buddy Pte Ltd',
                icon: '{{asset('img/marker.png')}}'
            });
        });
    } *///----------

    $(document).on('click', '#saveLongLat', function() {
        $.ajax({
            url: '/save-position',
            type: "post",
            data: {
                '_token': $('#_token').val(),
                /*'lat': $('#Lat').html(),
                'long': $('#Long').html()*/
                'link': $('#mapUrl').val()
            },
            success: function($response){
                location.reload();
            }
        });
    });
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap">
</script>
@endsection