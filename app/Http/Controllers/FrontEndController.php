<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 00:13
 */

namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\DB;
use App\Address;
use App\Representative;

class FrontEndController extends Controller{

    public function showCompanyPage($id) {
        $company = Representative::where('id', $id)->first();
        return view('frontend.company', ['page_title' => $company->title, 'company' => $company]);
    }

}