<style>
    h4 span {
        font-weight: 200;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <h4>Name: <span>{{$name}}</span></h4>
        <h4>Company: <span>{{$company}}</span></h4>
        <h4>Email: <span>{{$email}}</span></h4>
        <h4>Tel. No.: <span>{{$tel}}</span></h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p>{{$enquiry_text}}</p>
    </div>
</div>