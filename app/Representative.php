<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 14:19
 */

namespace App;



use Illuminate\Database\Eloquent\Model;

class Representative extends Model{

    protected $table = 'representatives';
    protected $fillable = ['title', 'logo', 'link_active', 'link', 'page_content'];

}