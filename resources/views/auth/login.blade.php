<!-- resources/views/auth/login.blade.php -->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SP Boeki Login to Admin Panel</title>
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
<link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">
<style>
    html,
    body {
        overflow-x: hidden;
    }
    .row {
        height: 100%;
        background-color: #d6d6d6;
        overflow-x: hidden;
    }
    .login-form {
        background-color: white;
        display: block;
        margin: 15% auto;
        width: 400px;
        border: 1px solid navy;
        border-radius: 5px;
        padding: 40px 30px 40px 30px;
    }
    .input-line label {
        display: inline-block;
        width: 30%;
    }
    .input-line input {
        display: inline-block;
        width: 60%;
        border: 1px solid #d3d3d3;
        border-radius: 3px;
    }
    .login-button {
        display: block;
        font-weight: 200;
    }
    .login-button:hover {

    }
    .landing {
        display: block;
        float: right;
        margin-top: -34px;
    }
</style>
</head>
<div class="row">
    <form method="POST" action="/auth/login" class="login-form">
        {!! csrf_field() !!}

        <div class="form-group">
            <label for="email">Username</label>
            <input class="form-control" type="text" name="name">
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="password" name="password" id="password">
        </div>

        <div class="form-group">
            <label><input type="checkbox" name="remember"> Remember Me</label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary login-button">Login</button>
            <a class="btn btn-success login-button landing" href="{{asset('/')}}">To Landing Page</a>
        </div>
        <?php $messages = $errors->all();
            if(count($messages) > 0){
                echo "<span style=\"color: red;\">".$messages[0]."</span>";
            }?>
    </form>
</div>