@extends('backend.master')
<style>
    .empty {
        display: block;
        margin: auto;
        width: 70%;
    }
    .empty label {
        margin-right: 40px;
    }
    .empty img {
        max-width: 300px;
        border: 1px solid grey;
    }
</style>
@section('content')
    <form class="empty">
        <div class="form-group">
            <label for="title">Title</label>
            <p>{{$company->title}}</p>
        </div>
        <div class="form-group">
            <label for="logo">Logo</label>
            <div id="image-cropper">
                <div class="cropit-image-preview"></div>
                <img src="{{asset('img/logos/'.$company->logo)}}">
            </div>
        </div>
        <div class="form-group">
            <label for="link">Link</label>
            <p>{{$company->link}}</p>
        </div>
        <div class="form-group">
            <label for="page_content">Internal Page</label>
            {!! $company->page_content !!}
        </div>
        <a href="{{asset('companies')}}" class="btn btn-warning">Go Back</a>
    </form>

@endsection