-- phpMyAdmin SQL Dump
-- version 4.4.4
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 20 2015 г., 00:08
-- Версия сервера: 5.6.24
-- Версия PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `invoicingbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `responders`
--

CREATE TABLE IF NOT EXISTS `responders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `inner_content` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `responders`
--

INSERT INTO `responders` (`id`, `title`, `inner_content`, `created_at`, `updated_at`) VALUES
(1, 'welcome', 'Welcome, User!', '2015-11-19 19:48:09', '2015-11-19 19:48:09'),
(2, 'unsubscribe', 'Unsubscribe user', '2015-11-19 19:48:09', '2015-11-19 19:48:09');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `responders`
--
ALTER TABLE `responders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `responders`
--
ALTER TABLE `responders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
