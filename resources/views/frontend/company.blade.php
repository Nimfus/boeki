<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>SP BOEKI</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">
    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href='{{asset('fonts/calibril.ttf')}}' rel='stylesheet' type='text/css'>

    <link href='{{asset('fonts/calibri_7.ttf')}}' rel='stylesheet' type='text/css'>
    <link href='{{asset('fonts/calibrib_5.ttf')}}' rel='stylesheet' type='text/css'>
    <link href='{{asset('fonts/calibril.ttf')}}' rel='stylesheet' type='text/css'>

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Scripts
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <style>
        @font-face {
            font-family: Calibri-Light;
            src: url(../fonts/calibril.ttf);
        }
        html,
        body {
            overflow-x: hidden;
            background: #f5f5f5;
            font-family: Calibri-Light;
            color: #696969;
            font-weight: 200;
            /*font-size: 16px;*/
            min-height:  calc(100vh - 150px);;
            /*height: 100%;*/

        }
        body {
            /*margin-bottom: 150px;*/
        }
        .wrapper {
            padding-bottom: 150px;
        }
        a:hover,
        a:visited,
        a:link,
        a:active,
        a:focus {
            text-decoration: none;
            background: inherit;

        }
        a:focus {
            background: white;
        }
        h1 {
            text-align: center;
        }
        .container-fluid {
            padding: 0;
            width:100%;
            min-height: calc(100vh - 150px);
            padding-bottom: 150px;
        }
        #content {
            min-height: calc(100vh - 150px);
        }
        .logo-responsive {
            display: block;
            margin: 15% auto;
        }
        .navigation {
            background: white;
            min-height: 70px;
            z-index: 10;
            margin: 0 auto;
            width: 100%;
            padding: 0;
        }
        .logo-img {
            max-height: 70px;
        }
        .navbar-right {
            width: 100%;
            text-align: center;
            display: block;
        }
        .navbar-nav li {
            width: 10%;
            min-height: 70px;
            text-align: center;
        }
        .top-menu-item:hover {
            background: #f5f5f5;
            transition: 0.3s;
        }
        .navbar-nav li a {
            padding: 0;
            min-height: 30px;
            color: slategrey;
            font-size: 18px;
        }
        .navbar-nav li a:hover {
            background: inherit;
        }
        .page-link {
            margin-top: 24px;
        }
        .top-menu-logo {
            margin: 0 10% 0 10%;
        }
        .header {
            position: relative;
            width: 100%;
            height: 100vh;
            text-align: center;
            color: #DFDDDD;
            background-image: url(../img/header-img.jpg);
            background-position: center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-size: cover;
            -o-background-size: cover;
            margin: 0;
        }

        .header .header-content {

        }

        .header .header-content .header-content-inner h1 {
            margin-top: 0;
            margin-bottom: 0;
            text-transform: uppercase;
            font-weight: 700;
        }

        .header .header-content .header-content-inner hr {
            margin: 30px auto;
        }

        .header .header-content .header-content-inner p {
            margin-bottom: 50px;
            font-size: 16px;
            font-weight: 300;
            color: rgba(255,255,255,.7);
        }
        /* ---------------------------------------------*/
        /* About us */
        .about-us {
            margin-top: 70px;
            padding: 0 20% 0 20%;
        }
        .about-us ul {
            list-style: square outside;
        }
        /* ---------------------------------------------*/
        
        .divider {
            width: 80%;
            border-color: #c1c1c1;
        }

        i {
            color: #1e6ea4;
            font-size: 18px;
        }
        .footer-img {
            display: block;
            margin: 0 auto;
            height: 80%;
        }
        footer {
            padding-top: 20px;
            height: 150px;
            background: #303030;
            margin-top: 50px;
        }
        .created-by-logo-string {
            display: block;
            margin: 0;
            float: right;
            margin-right: 10%;
            color: white;
            font-size: 12px;
        }
        .created-by-logo-string span {
            position: relative;
            top: 2px;
        }
        .created-by-logo-string img {
            height: 20px;
        }
        @media screen and (max-width: 762px) {
            .about-us {
                margin-top: 20px;
                padding: 0 20% 0 20%;
            }
            .shift {
                text-align: left;
            }
            .form-group label {
                float: left;
            }
            #send-btn {
                margin: auto;
            }
            .logo-responsive {
                max-width: 100%;
                /*display: none;*/
            }
            .navigation {
                background: white;
                min-height: 0;
                z-index: 10;
                margin: 0;
                width: 110%;
                padding: 0;
                text-align: center;
            }
            .logo-img {
                display: none;
            }
            .navbar-right {
                width: 100%;
                text-align: center;
                display: block;
            }
            .navbar-collapse {

            }
            .navbar-nav {
                margin: 0;
            }
            .navbar-nav li {
                width: 25%;
                min-height: 0;
                text-align: center;
                display: inline-block;
                margin-left: -12px;
            }
            .top-menu-item:hover {
                background: #f5f5f5;
                transition: 0.3s;
            }
            .navbar-nav li a {
                padding: 0;
                min-height: 0;
                color: slategrey;
            }
            .navbar-nav li a:hover {
                background: inherit;
            }
            .page-link {
                margin-top: 0;
            }
            .top-menu-logo {
                max-height: 0;
                max-width: 0;
                margin: 0;
                padding: 0;
                display: none;
            }
            footer {
                padding-top: 20px;
                height: 230px;
                background: #303030;
                margin-top: 50px;
            }
            .created-by-logo-string {
                display: block;
                margin: auto;
                width: 100%;
                text-align: center;
                padding-top: 15px;
            }
            .created-by-logo-string span {
                position: relative;
                top: 2px;
            }
            .created-by-logo-string img {
                height: 20px;
            }
        }

    </style>
</head>
<body>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container-fluid">
    <div class="navbar-collapse navbar-fixed-top navigation">
        <ul class="nav navbar-nav navbar-right">
            <li class="top-menu-logo">
                <a href="{{asset('/')}}"><img class="logo-img" src="{{asset('img/logo.png')}}"></a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="{{asset('/')}}">Home</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="{{asset('/')}}">About Us</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="{{asset('/')}}">Products</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="{{asset('/')}}">Contact</a>
            </li>
        </ul>
    </div>

    <div class="row about-us" id="content">
        <h1>{{$company->title}}</h1>
        {!! $company->page_content !!}
    </div>

</div>
<footer>
    <img class="footer-img" src="{{asset('img/footer-img.png')}}">
    <p class="created-by-logo-string"><span>Created by</span>  <a href="https://businessbuddy.sg/"><img class="managed-by" src="{{asset('img/created-by-logo.png')}}"></a></p>
</footer>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>