@extends('backend.master')
<style>
    form {
        margin: 0 30% 0 10%;
    }
    #page_content {
        height: 600px;
        resize: none;
    }
    .company-img {
        width: 40%;
    }
</style>
@section('content')
    <form method="POST" action="/companies/store" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" class="form-control" name="old_title" id="old_title" @if($company!=null) value="{{$company->title}}" @endif>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Company Title" @if($company!=null) value="{{$company->title}}" @endif>
        </div>
        <div class="form-group">
            <label for="logo">Logo</label>
                <input type="file"  name="logo" id="logo">
                <img class="company-img" src="{{asset('img/logos/'.$company->logo)}}">
        </div>
        <div class="form-group">
            <label for="link_active">
                <input type="checkbox" id="link_active" name="link_active" value="1" @if($company!=null && $company->link!=null) checked @endif> Use external link
            </label>
        </div>
        <div class="form-group">
            <label for="link">Link</label>
            <input type="text" class="form-control" name="link" id="link" placeholder="External Link" @if($company!=null && $company->link!=null) value="{{$company->link}}" @endif>
        </div>
        <div class="form-group">
            <label for="page_content">Internal Page Editor</label>
            <textarea class="form-control" name="page_content" id="page_content" placeholder="Create Page Here">@if($company!=null && $company->page_content!=null) {{$company->page_content}} @endif</textarea>
        </div>
        <button type="submit" class="btn btn-default">Save</button>
    </form>

    <script>
        $(document).ready(function() {
            var pageContent = CKEDITOR.replace('page_content', {
                extraPlugins: 'button,panelbutton,colorbutton,colordialog,uicolor,uploadimage,font,listblock,justify,liststyle,contextmenu,menu,floatpanel,panel,dialogui',
                height: 300,

                // Upload images to a CKFinder connector (note that the response type is set to JSON).
                uploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                filebrowserBrowseUrl: '../js/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '../js/ckfinder/ckfinder.html?type=Images',
                filebrowserUploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

                // The following options are not necessary and are used here for presentation purposes only.
                // They configure the Styles drop-down list and widgets to use classes.

                stylesSet: [
                    { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
                    { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
                ],

                // Load the default contents.css file plus customizations for this sample.
                contentsCss: [ CKEDITOR.basePath + 'contents.css', 'http://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ],

                // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
                // resizer (because image size is controlled by widget styles or the image takes maximum
                // 100% of the editor width).
                image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
                image2_disableResizer: true
            });

            if ($('#link_active').is(':checked')) {
                $('#link').prop("disabled", false);
                //$('#page_content').setReadOnly(true);
            } else {
                $('#link').prop("disabled", true);
                //$('#page_content').setReadOnly(false);
            }

            $(document).on('change', '#link_active', function () {
                if ($('#link_active').is(':checked')) {
                    $('#link').prop("disabled", false);
                    pageContent.setReadOnly(true);
                } else {
                    $('#link').prop("disabled", true);
                    $('#page_content').prop("disabled", false);
                    pageContent.setReadOnly(false);
                }
            });
        });

    </script>
@endsection
