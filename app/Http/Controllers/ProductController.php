<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 19:09
 */

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Product;

class ProductController extends Controller {

    public function store(ProductRequest $request) {
        $product = Product::where('title', $request->old_title)->first();

        if($product!=null){
            $product->title = $request->title;
            $product->description = $request->description;

            $product->save();

            if($_REQUEST['image1']!=null){
                $image1 = $_REQUEST['image1'];
                //Extention
                $exp1 = explode(';', $image1);
                $exp1 = explode(':', $exp1[0]);
                $image = array_pop($exp1);
                $extention = explode('/', $image);
                $extention = $extention[count($extention) -1];
                /***/

                $exp = explode(',', $image1);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $file = $product->id.'_1.'.$extention;
                $product->image1 = $file;
                $product->save();
                //file_put_contents(base_path() . '/public/img/products/'.$file, $data);
                file_put_contents('../public/img/products/'.$file, $data);
            }

            if($_REQUEST['image2']!=null){
                $image1 = $_REQUEST['image2'];
                //Extention
                $exp1 = explode(';', $image1);
                $exp1 = explode(':', $exp1[0]);
                $image = array_pop($exp1);
                $extention = explode('/', $image);
                $extention = $extention[count($extention) -1];
                /***/

                $exp = explode(',', $image1);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $file = $product->id.'_2.'.$extention;
                $product->image2 = $file;
                $product->save();
                //file_put_contents(base_path() . '/public/img/products/'.$file, $data);
                file_put_contents('../public/img/products/'.$file, $data);
            }

            if($_REQUEST['image3']!=null){
                $image1 = $_REQUEST['image3'];
                //Extention
                $exp1 = explode(';', $image1);
                $exp1 = explode(':', $exp1[0]);
                $image = array_pop($exp1);
                $extention = explode('/', $image);
                $extention = $extention[count($extention) -1];
                /***/

                $exp = explode(',', $image1);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $file = $product->id.'_3.'.$extention;
                $product->image3 = $file;
                $product->save();
                //file_put_contents(base_path() . '/public/img/products/'.$file, $data);
                file_put_contents('../public/img/products/'.$file, $data);
            }
            /*if ($request->file('image1')!=null){
                $imageName = $product->id . '_1.' . $request->file('image1')->getClientOriginalExtension();
                $product->image1 = $product->id . '_1.' . $request->file('image1')->getClientOriginalExtension();

                $product->save();
                $request->file('image1')->move(base_path() . '/public/img/products/', $imageName);
                //$request->file('attached_image')->move('../reis/img/forms/cover-forms/', $imageName);
            }

            if ($request->file('image2')!=null){
                $imageName = $product->id . '_2.' . $request->file('image2')->getClientOriginalExtension();
                $product->image2 = $product->id . '_2.' . $request->file('image2')->getClientOriginalExtension();

                $product->save();
                $request->file('image2')->move(base_path() . '/public/img/products/', $imageName);
                //$request->file('attached_image')->move('../reis/img/forms/cover-forms/', $imageName);
            }

            if ($request->file('image3')!=null){
                $imageName = $product->id . '_3.' . $request->file('image3')->getClientOriginalExtension();
                $product->image3 = $product->id . '_3.' . $request->file('image3')->getClientOriginalExtension();

                $product->save();
                $request->file('image3')->move(base_path() . '/public/img/products/', $imageName);
                //$request->file('attached_image')->move('../reis/img/forms/cover-forms/', $imageName);
            }*/

            $products = Product::all();
            //return view('backend.products.view', ['page_title' => 'Products', 'products' => $products]);

            //return redirect('products/edit/'.$product->id);

        }else {
            $product = new Product([
                'title' => $request->title,
                'description' => $request->description
            ]);

            $product->save();

            if($_REQUEST['image1']!=null){
                $image1 = $_REQUEST['image1'];
                //Extention
                $exp1 = explode(';', $image1);
                $exp1 = explode(':', $exp1[0]);
                $image = array_pop($exp1);
                $extention = explode('/', $image);
                $extention = $extention[count($extention) -1];
                /***/

                $exp = explode(',', $image1);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $file1 = $product->id.'_1.'.$extention;
                $product->image1 = $file1;
                $product->save();
                //file_put_contents(base_path() . '/public/img/products/'.$file1, $data);
                file_put_contents('../public/img/products/'.$file1, $data);
            }

            if($_REQUEST['image2']!=null){
                $image1 = $_REQUEST['image2'];
                //Extention
                $exp1 = explode(';', $image1);
                $exp1 = explode(':', $exp1[0]);
                $image = array_pop($exp1);
                $extention = explode('/', $image);
                $extention = $extention[count($extention) -1];
                /***/

                $exp = explode(',', $image1);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $file2 = $product->id.'_2.'.$extention;
                $product->image2 = $file2;
                $product->save();
                //file_put_contents(base_path() . '/public/img/products/'.$file2, $data);
                file_put_contents('../public/img/products/'.$file2, $data);
            }

            if($_REQUEST['image3']!=null){
                $image1 = $_REQUEST['image3'];
                //Extention
                $exp1 = explode(';', $image1);
                $exp1 = explode(':', $exp1[0]);
                $image = array_pop($exp1);
                $extention = explode('/', $image);
                $extention = $extention[count($extention) -1];
                /***/

                $exp = explode(',', $image1);
                $base64 = array_pop($exp);
                $data = base64_decode($base64);
                $file3 = $product->id.'_3.'.$extention;
                $product->image3 = $file3;
                $product->save();
                //file_put_contents(base_path() . '/public/img/products/'.$file3, $data);
                file_put_contents('../public/img/products/'.$file3, $data);
            }
            /*if ($request->file('image1')!=null){
                $imageName = $product->id . '_1.' . $request->file('image1')->getClientOriginalExtension();
                $product->image1 = $product->id . '_1.' . $request->file('image1')->getClientOriginalExtension();

                $product->save();
                $request->file('image1')->move(base_path() . '/public/img/products/', $imageName);
                //$request->file('attached_image')->move('../reis/img/forms/cover-forms/', $imageName);
            }

            if ($request->file('image2')!=null){
                $imageName = $product->id . '_2.' . $request->file('image2')->getClientOriginalExtension();
                $product->image2 = $product->id . '_2.' . $request->file('image2')->getClientOriginalExtension();

                $product->save();
                $request->file('image2')->move(base_path() . '/public/img/products/', $imageName);
                //$request->file('attached_image')->move('../reis/img/forms/cover-forms/', $imageName);
            }

            if ($request->file('image3')!=null){
                $imageName = $product->id . '_3.' . $request->file('image3')->getClientOriginalExtension();
                $product->image3 = $product->id . '_3.' . $request->file('image3')->getClientOriginalExtension();

                $product->save();
                $request->file('image3')->move(base_path() . '/public/img/products/', $imageName);
                //$request->file('attached_image')->move('../reis/img/forms/cover-forms/', $imageName);
            }*/

            $products = Product::all();
            //return view('backend.products.view', ['page_title' => 'Products', 'products' => $products]);

            //return redirect('products/edit/'.$product->id);
        }
    }

}