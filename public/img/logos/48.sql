-- phpMyAdmin SQL Dump
-- version 4.4.4
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 20 2015 г., 00:08
-- Версия сервера: 5.6.24
-- Версия PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `invoicingbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `mail_template`
--

CREATE TABLE IF NOT EXISTS `mail_template` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `template_content` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `mail_template`
--

INSERT INTO `mail_template` (`id`, `title`, `template_content`, `created_at`, `updated_at`) VALUES
(2, 'Test Template', '<h1><u>Heading Of Message</u></h1>\n                      <h4>Subheading</h4>\n                      <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\n                          was born and I will give you a complete account of the system, and expound the actual teachings\n                          of the great explorer of the truth, the master-builder of human happiness. No one rejects,\n                          dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know\n                          how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again\n                          is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,\n                          but because occasionally circumstances occur in which toil and pain can procure him some great\n                          pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,\n                          except to obtain some advantage from it? But who has any right to find fault with a man who\n                          chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that\n                          produces no resultant pleasure? On the other hand, we denounce with righteous indignation and\n                          dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so\n                          blinded by desire, that they cannot foresee</p>\n                      <ul>\n                          <li>List item one</li>\n                          <li>List item two</li>\n                          <li>List item three</li>\n                          <li>List item four</li>\n                      </ul>\n                      <p>Thank you,</p>\n                      <p>John Doe</p>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Test Template 2', '<h1><u>Heading Of Message</u></h1>\n                      <h4><br></h4><ul>\n                      </ul>\n                      <p>Thank you,</p>\n                      <p>John Doe</p>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Test Template 5', '<p>Hi there!</p>', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `mail_template`
--
ALTER TABLE `mail_template`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `mail_template`
--
ALTER TABLE `mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
