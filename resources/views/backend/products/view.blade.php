@extends('backend.master')
<style>
    .btn-success,
    .table-striped {
        margin-left: 10%;
    }
    table a {
        margin-right: 20px;
    }
    .edit,
    .delete,
    .view {
        padding-left: 10px;
    }
    .edit:hover {
        color: purple;
    }
    .delete:hover {
        color: red;
    }
    .view:hover {
        color: green;
    }
    td a {
        text-align: center;
    }
    .sign-out-button {
        display: block;
        height: 24px;
        padding-top: 0;
        float: right;
        transition: 0.3s;
        position: relative;
        top: -20px;
    }
</style>
@section('content')
    <table class="table table-striped" style="width: 80%;">
        <thead><td>ID</td><td>Title</td><td>View</td><td>Edit</td><td>Remove</td></thead>
        <tbody>
        @foreach($products as $product)
        <tr class="tdT"><td>{{$product->id}}</td><td>{{$product->title}}</td><td><a href="{{asset('products/view/'.$product->id)}}"><i class="fa fa-eye view"></i></a></td><td><a href="{{asset('products/edit/'.$product->id)}}"><i class="fa fa-edit edit"></i></a></td><td><a href="{{asset('products/remove/'.$product->id)}}"><i class="fa fa-trash-o delete"></i></a></td></tr>
        @endforeach
        </tbody>
    </table>
    <a class="btn btn-success" href="{{asset('products/new')}}">Create New</a>
@endsection