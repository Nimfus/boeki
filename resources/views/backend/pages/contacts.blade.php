@extends('backend.master')
<style>
    .form-group {
        width: 80%;
    }
    .form-group input {
        width: 60%;
    }
    .remarks {
        text-align: center;
    }
</style>
@section('content')
    <form class="form-horizontal" action="/contacts/store" method="POST">
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="col-sm-3 control-label" for="company_name">Company Name:</label>
            <div class="col-sm-9">
                <input type="text" name="company_name" @if($address!=null) value="{{$address->company_name}}" @endif>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="address_1">Address Line 1</label>
            <div class="col-sm-9">
                <input type="text" name="address_1" @if($address!=null) value="{{$address->address_1}}" @endif>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="address_2">Address Line 2</label>
            <div class="col-sm-9 ">
                <input type="text" name="address_2" @if($address!=null) value="{{$address->address_2}}" @endif>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="tel_1">Phone No. 1</label>
            <div class="col-sm-9">
                <input type="text" name="tel_1"  @if($address!=null) value="{{$address->tel_1}}" @endif>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="tel_2">Phone No. 2</label>
            <div class="col-sm-9">
                <input type="text" name="tel_2" @if($address!=null) value="{{$address->tel_2}}" @endif>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="fax">Fax No.:</label>
            <div class="col-sm-9">
                <input type="text" name="fax" @if($address!=null) value="{{$address->fax}}" @endif>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="email">Email:</label>
            <div class="col-sm-9">
                <input class="file-input" type="email" name="email" @if($address!=null) value="{{$address->email}}" @endif>
            </div>
        </div>
        <div class="remarks">
            <div class="col-sm-12">
                <input class="submit-button btn btn-default" type="submit" value="Save">
            </div>
        </div>
    </form>
    <script>
        CKEDITOR.replace('editor', {
            extraPlugins: 'button,panelbutton,colorbutton,colordialog,uicolor,uploadimage,font,listblock,justify,liststyle,contextmenu,menu,floatpanel,panel,dialogui',
            height: 300,

            // Upload images to a CKFinder connector (note that the response type is set to JSON).
            uploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

            // Configure your file manager integration. This example uses CKFinder 3 for PHP.
            filebrowserBrowseUrl: '../js/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '../js/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '../js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

            // The following options are not necessary and are used here for presentation purposes only.
            // They configure the Styles drop-down list and widgets to use classes.

            stylesSet: [
                { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
                { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
            ],

            // Load the default contents.css file plus customizations for this sample.
            contentsCss: [ CKEDITOR.basePath + 'contents.css', 'http://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ],

            // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
            // resizer (because image size is controlled by widget styles or the image takes maximum
            // 100% of the editor width).
            image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
            image2_disableResizer: true
        });

    </script>
@endsection

