<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 19:05
 */

namespace App\Http\Controllers;

use App\Http\Requests\RepresentativeRequest;
use App\Representative;

class RepresentativeController extends Controller {

    public function store(RepresentativeRequest $request) {
        $company = Representative::where('title', $request->old_title)->first();

        if($company!=null){
            $company->title = $request->title;
            $company->link_active = $request->link_active;
            $company->link = $request->link;
            $company->page_content = $request->page_content;

            $company->save();

            if ($request->file('logo')!=null){
                $imageName = $company->id . '_logo.' . $request->file('logo')->getClientOriginalExtension();
                $company->logo = $company->id . '_logo.' . $request->file('logo')->getClientOriginalExtension();

                $company->save();
                //$request->file('logo')->move(base_path() . '/public/img/logos/', $imageName);
                $request->file('logo')->move('../public/img/logos/', $imageName);
            }

            $companies = Representative::all();
            return redirect('companies');

        }else {
            $company = new Representative([
                'title' => $request->title,
                'link_active' => $request->link_active,
                'link' => $request->link,
                'page_content' => $request->page_content,
            ]);

            $company->save();

            if ($request->file('logo')!=null){
                $imageName = $company->id . '_logo.' . $request->file('logo')->getClientOriginalExtension();
                $company->logo = $company->id . '_logo.' . $request->file('logo')->getClientOriginalExtension();

                $company->save();
                //$request->file('logo')->move(base_path() . '/public/img/logos/', $imageName);
                $request->file('logo')->move('../public/img/logos/', $imageName);
            }

            $companies = Representative::all();
            return redirect('companies');
        }
    }

}