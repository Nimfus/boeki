@extends('backend.master')
<style>
    .empty {
        display: block;
        margin: auto;
        width: 70%;
    }
    .empty label {
        margin-right: 40px;
    }
    .empty img {
        max-width: 300px;
        border: 1px solid grey;
    }
</style>
@section('content')
    <form class="empty">
        <div class="form-group">
            <label for="title">Title:</label>
            <p>{{$product->title}}</p>
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <p>{{$product->description}}</p>
        </div>
        <div class="form-group">
            <label for="image1">First Image:</label>
            <img src="{{asset('img/products/'.$product->image1)}}">
        </div>
        <div class="form-group">
            <label for="image2">Second Image:</label>
            <img src="{{asset('img/products/'.$product->image2)}}">
        </div>
        <div class="form-group">
            <label for="image3">Third Image:</label>
            <img src="{{asset('img/products/'.$product->image3)}}">
        </div>
        <a href="{{asset('products')}}" class="btn btn-warning">Go Back</a>
    </form>
@endsection