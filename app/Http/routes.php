<?php

use App\Address;
use App\About;
use App\Representative;
use App\Product;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*Authentication and registration routes*/
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');

//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');
/*===================================================*/
Route::get('/', function () {
    $address = Address::where('id', 1)->first();
    $about = About::where('id', 1)->first();
    $companies = Representative::all();
    $products = Product::all();
    $map = DB::table('map')->where('id', 1)->first();
    return view('frontend.master', ['address' => $address, 'about' => $about, 'companies' => $companies, 'products' => $products, 'map' => $map]);
});
Route::get('home', function () {
    $address = Address::where('id', 1)->first();
    $about = About::where('id', 1)->first();
    $companies = Representative::all();
    $products = Product::all();
    $map = DB::table('map')->where('id', 1)->first();
    return view('frontend.master', ['address' => $address, 'about' => $about, 'companies' => $companies, 'products' => $products, 'map' => $map]);
});

Route::get('company/{id}', 'FrontEndController@showCompanyPage');
Route::post('send', 'EnquiryController@send');


Route::group(['middleware' => 'role:admin'], function() {
    Route::get('admin', function () {
        return redirect('about-us');
    });
    Route::get('map', 'BackEndController@showMap');
    Route::post('save-position', 'BackEndController@savePosition');
    Route::get('contact-us', 'BackEndController@showContacts');
    Route::get('about-us', 'BackEndController@showAboutUs');
    Route::get('products', 'BackEndController@showAllProducts');
    Route::get('products/new', 'BackEndController@createNewProduct');
    Route::get('products/edit/{id}', 'BackEndController@editProduct');
    Route::get('products/remove/{id}', 'BackEndController@removeProduct');
    Route::get('products/view/{id}', 'BackEndController@showProduct');
    Route::get('companies', 'BackEndController@showAllCompanies');
    Route::get('new', 'BackEndController@createNewCompany');
    Route::get('edit/{id}', 'BackEndController@editCompany');
    Route::get('companies/remove/{id}', 'BackEndController@removeCompany');
    Route::get('companies/view/{id}', 'BackEndController@showCompany');

    Route::post('contacts/store', 'ContactController@store');
    Route::post('about-us/store', 'AboutController@store');
    Route::post('products/store', 'ProductController@store');
    Route::post('companies/store', 'RepresentativeController@store');

    Route::get('profile', 'BackEndController@showProfile');
    Route::post('password-change', 'UserController@changePassword');
});