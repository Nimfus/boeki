<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = ['send', 'map', 'save-position', 'products/store', 'password-change', 'connector.php?command=QuickUpload&type=Files',
    'connector.php?command=QuickUpload&type=Images', 'ckfinder.html?type=Images', 'connector.php?command=QuickUpload&type=Files&responseType=json', '/ckfinder.html'];
}
