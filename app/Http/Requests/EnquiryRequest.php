<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 18.11.15
 * Time: 16:10
 */

namespace App\Http\Requests;


class EnquiryRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'title' => 'required|string|min:3',
            'name' => 'required|string|min:3',
            'company' => 'required|string|min:3',
            'email' => 'required|email',
            'tel' => 'required|string|min:3',
            'feedback' => 'required|min:3',
            'drawing' => 'mimes:jpg,bmp,png,jpeg,pdf,excel,csv,vnd.ms-excel,vnd.msexcel,doc,docx,xls,xlsx|max:10000',
        ];
    }

}