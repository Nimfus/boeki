<?php
/**
 * Created by PhpStorm.
 * User: Nimfus
 * Date: 14.11.15
 * Time: 14:38
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class About extends Model {

    protected $table = 'about_info';
    public $fillable = ['contents'];

}