<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{asset('/')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">SP BOEKI</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">SP BOEKI</span>
    </a>
<style>
    @font-face {
        font-family: Calibri-Light; /* Имя шрифта */
        src: url(fonts/calibril.ttf); /* Путь к файлу со шрифтом */
    }
    .sign-out-button {
        display: block;
        height: 24px;
        padding-top: 0;
        float: right;
        transition: 0.3s;
    }
    .sign-out-button:hover {
        transition: 0.3s;
    }
    .user-name {
        display: block;
        float: left;
        margin-right: 20px;
    }
    .navbar-custom-menu {
        margin-right: 40px;
        padding-top: 12px;
        display:block;
        max-height: 36px;
    }
    .pull-right {
        margin-top: -5px;
    }
    .pull-right p span {
        display: inline-block;
        position: relative;
        top:2px;
    }
    .managed-by {
        height: 30px;
    }
</style>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <span class="hidden-xs user-name">{{Auth::user()->name}}</span>
            <a href="{{asset('logout')}}" class="btn btn-success sign-out-button">Sign out</a>
        </div>
    </nav>
</header>