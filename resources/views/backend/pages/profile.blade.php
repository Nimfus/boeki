@extends('backend.master')

<style>
    .modal-body {
        text-align: center;
    }
    .btn-success {
        margin-top: 15px;
    }
    .sign-out-button {
        display: block;
        height: 24px;
        padding-top: 0;
        float: right;
        transition: 0.3s;
        position: relative;
        top: -15px;
    }
</style>

@section('content')
    <div class="modal fade" id="passwordChanged">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Password Changes</h4>
                </div>
                <div class="modal-body">
                    <p>Password was successfully changed</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="emailChanged">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Email Changes</h4>
                </div>
                <div class="modal-body">
                    <p>Email was successfully changed</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- This will be prototype for registration form -->
    <div class="col-xs-6">
        <form method="POST">
            <input type="hidden" id="_token" value="{{ csrf_token() }}">
            <h4>Change password</h4>
            <div class="fs">
                <label for="old_password">Old Password:</label>
                <input class="form-control" id="old_password" type="password" name="old_password">
            </div>
            <div class="fs">
                <label for="new_password">New Password:</label>
                <input class="form-control" id="new_password" type="password" name="new_password">
            </div>

            <div class="fs">
                <label for="new_password_confirmation">New Password Confirmation:</label>
                <input class="form-control" id="password_confirmation" type="password" name="new_password_confirmation">
            </div>
            <div>
                <button id="change-password" type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
    <script>
        $(document).ready(function() {
            //Password change
            $(document).on('click', '#change-password', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '/password-change',
                    type: "post",
                    data: {
                        '_token': $('#_token').val(),
                        'password': $('#old_password').val(),
                        'new_password': $('#new_password').val(),
                        'password_confirmation': $('#password_confirmation').val()
                    },
                    success: function($response){
                        $('#old_password').val('');
                        $('#new_password').val('');
                        $('#password_confirmation').val('');
                        $('#passwordChanged').modal('show');
                    }
                });
            });
        });
    </script>
@endsection