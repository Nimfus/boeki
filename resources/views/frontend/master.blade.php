<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>SP BOEKI</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <!--meta name="_token" content="{!! csrf_token() !!}"/-->
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">
    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">
    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href='{{asset('fonts/calibril.ttf')}}' rel='stylesheet' type='text/css'>

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Scripts
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">

<style>
    @font-face {
        font-family: Calibri-Light;
        src: url(fonts/calibril.ttf);
    }
    html,
    body {
        overflow-x: hidden;
        background: #f5f5f5;
        font-family: Calibri-Light;
        color: #696969;
        font-weight: 200;
        font-size: 18px;
    }
    a:hover,
    a:visited,
    a:link,
    a:active,
    a:focus {
        text-decoration: none;
        background: inherit;
    }
    a:focus {
        background: white;
    }
    h1 {
        text-align: center;
    }
    .container-fluid {
        padding: 0;
        font-family: Calibri-Light;
    }
    .logo-responsive {
        display: block;
        margin: 15% auto;
    }
    .navigation {
        background: white;
        min-height: 70px;
        z-index: 10;
        margin: 0 auto;
        width: 100%;
        padding: 0;
    }
    .logo-img {
        max-height: 70px;
    }
    .navbar-right {
        width: 100%;
        text-align: center;
        display: block;
    }
    .navbar-nav li {
        width: 10%;
        min-height: 70px;
        text-align: center;
    }
    .top-menu-item:hover {
        background: #f5f5f5;
        transition: 0.3s;
    }
    .navbar-nav li a {
        padding: 0;
        min-height: 30px;
        color: slategrey;
    }
    .navbar-nav li a:hover {
        background: inherit;
    }
    .page-link {
        margin-top: 24px;
    }
    .top-menu-logo {
        margin: 0 10% 0 10%;
    }
    .header {
        position: relative;
        width: 100%;
        height: 100vh;
        text-align: center;
        color: #DFDDDD;
        background-image: url(../img/header-img.jpg);
        background-position: center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
        margin: 0;
    }

    .header .header-content {

    }

    .header .header-content .header-content-inner h1 {
        margin-top: 0;
        margin-bottom: 0;
        text-transform: uppercase;
        font-weight: 700;
    }

    .header .header-content .header-content-inner hr {
        margin: 30px auto;
    }

    .header .header-content .header-content-inner p {
        margin-bottom: 50px;
        font-size: 18px;
        font-weight: 300;
        color: rgba(255,255,255,.7);
    }
    /* ---------------------------------------------*/
    /* About us */
    .about-us {
        margin-top: 1px;
        text-align: center;
        padding: 0 20% 30px 20%;
        font-family: Calibri-Light;
    }
    .about-us ul {
        width: 60%;
        margin: auto;
        list-style: square outside;
        text-align: left;
    }
    .about-us h1 {
        margin: 60px 0 40px 0;
    }
    .about-us h4 {
        margin: 30px auto;
    }
    /* ---------------------------------------------*/

    /* Products */
    .products {
        margin: 20px 0 0 0;
        padding: 30px 12% 0 12%;
        background: white;
        font-family: Calibri-Light;
    }
    .product-row p {
        font-family: Calibri-Light;

        white-space: pre-wrap;
    }
    .product-row {
        margin-top: 20px;
    }
    .product-descr {
        width: 50%;
    }
    .products img {
        height: 160px;
        width: 100%;
    }
    .representative {
        padding: 20px 10% 40px 10%;
        background: white;
    }
    .products h4 {
        margin-top: 20px;
        color: #2688cb;
    }
    .product-div {

        width: 190px;
        height: 160px;
    }


    #waiting {
        position: fixed;
        display: block;
        margin: auto;
        background: transparent;
        top: 42%;
        left: 43%;
        z-index: 1000;
        height: 100px;
        width: 100px;
    }
    #waiting img {
        height: 150px;
    }

    footer {
        padding-top: 20px;
        height: 150px;
        background: #303030;
        margin-top: 50px;
    }
    .created-by-logo-string {
        display: block;
        margin: 0;
        float: right;
        margin-right: 10%;
        color: white;
        font-size: 12px;
    }
    .created-by-logo-string span {
        position: relative;
        top: 2px;
    }
    .created-by-logo-string img {
        height: 20px;
    }
    @media screen and (min-width: 1479px) {
        .product-div-r {
            margin-top: 20px;
            height: auto;
            float:right;
        }
        .products img {
            width: 100%;
        }
    }
    @media screen and (max-width: 1478px) {
        .product-descr {
            width: 45%;
        }
        .product-div-r {
            margin-top: 20px;
            height: auto;
            float:right;
        }
    }
    @media screen and (max-width: 1342px) {
        .product-descr {
            width: 40%;
        }
        .product-div-r {
            margin-top: 20px;
            height: auto;
            float:right;
        }
    }
    @media screen and (max-width: 1270px) {
        .products {
            text-align: left;
        }
        .product-descr {
            width: 40%;
        }
        .product-div {
            margin-top: 0;
            width: 190px;
        }
        .products img {
            height: 160px;
            width: 100%;
        }
        .product-div-r {
            margin-top: 0;
            max-height: 200px;
            float:right;
        }
    }
    @media screen and (max-width: 1200px) {
        .products {
            text-align: center;
        }
        .product-descr {
            width: 33%;
        }
        .product-div {
            margin-top: 20px;
            width: 190px;
        }
        .products img {
            height: 160px;
            width: 100%;
        }
        .product-div-r {
            margin: auto;
            height: auto;
            float:left;
        }
    }
    @media screen and (max-width: 1138px) {
        .product-descr {
            width: 25%;
        }
        .product-div-r {
            margin: auto;
            height: auto;
            float:right;
        }
        .products {
            text-align: center;
        }
        .product-div {
            width: 160px;
            margin: auto;
        }
        .products img {
            height: 130px;
            width: 100%;
        }
    }
    @media screen and (max-width: 992px) {
        .products {
            text-align: center;
        }
        .product-descr {
            width: 100%;
        }
        .product-div {
            width: 190px;
            margin: 0;
        }
        .products img {
            height: 160px;
            width: 100%;
        }
    }
    @media screen and (max-width: 762px) {
        #map {
            min-width: 100%;
        }
        .products {
            text-align: center;
        }
        .product-div {
            width: 160px;
            margin:20px auto;
        }
        .product-div-r {
            margin: 40px auto;
            height: auto;
            float:inherit;
        }
        .products img {
            height: 130px;
            width: 100%;
        }
        .r-logo {
            margin: auto;
            text-align: center;
        }
        /*.navbar-fixed-top {
            display: none;
        }*/
        .representative {
            text-align: center;
        }
        .enquiry label {
            text-align: left;
        }
        .contact-us {
            padding-left: 10%;
            padding-right: 10%;
        }
        .shift {
            text-align: left;
        }
        .form-group label {
            float: left;
        }
        #send-btn {
            margin: auto;
        }
        .logo-responsive {
            max-width: 100%;
            /*display: none;*/
        }
        .navigation {
            background: white;
            min-height: 0;
            z-index: 10;
            margin: 0;
            width: 110%;
            padding: 0;
            text-align: center;
        }
        .logo-img {
            display: none;
        }
        .navbar-right {
            width: 100%;
            text-align: center;
            display: block;
        }
        .navbar-collapse {

        }
        .navbar-nav {
            margin: 0;
        }
        .navbar-nav li {
            width: 25%;
            min-height: 0;
            text-align: center;
            display: inline-block;
            margin-left: -12px;
        }
        .top-menu-item:hover {
            background: #f5f5f5;
            transition: 0.3s;
        }
        .navbar-nav li a {
            padding: 0;
            min-height: 0;
            color: slategrey;
        }
        .navbar-nav li a:hover {
            background: inherit;
        }
        .page-link {
            margin-top: 0;
        }
        .top-menu-logo {
            max-height: 0;
            max-width: 0;
            margin: 0;
            padding: 0;
            display: none;
        }

        footer {
            padding-top: 20px;
            height: 230px;
            background: #303030;
            margin-top: 50px;
        }
        .created-by-logo-string {
            display: block;
            margin: auto;
            width: 100%;
            text-align: center;
            padding-top: 15px;
        }
        .created-by-logo-string span {
            position: relative;
            top: 2px;
        }
        .created-by-logo-string img {
            height: 20px;
        }

        #waiting {
            position: fixed;
            display: block;
            margin: 0 auto;
            top: 40%;
            left: inherit;
            right: inherit;
            text-align: center;
            background: transparent;
            z-index: 1000;
            height: 100px;
            width: 100%;
        }
        #waiting img {
            display: block;
            margin: auto;
            height: 150px;
        }

    }
    @media screen and (max-width: 500px) {
        .about-us {
            margin-top: 1px;
            text-align: center;
            padding: 0 10% 30px 10%;
            font-family: Calibri-Light;
        }
        .about-us ul {
            width: 90%;
            margin: auto;
            list-style: square outside;
            text-align: left;
        }
    }
    /* ---------------------------------------------*/

    /* Representatives */
    .representative h1 {
        margin-bottom: 50px;
    }
    .r-logo {
        margin-bottom: 15px;

        height: auto;
    }
    .r-logo img {
        display: block;
        margin: 0 auto;
        max-height: 100%;
        max-width: 100%;
    }
    /* ---------------------------------------------*/

    /* Contact Us */
    #map {
        width: 75%;
        height: 350px;
    }
    iframe {
        width: 100%;
        height: 350px;
    }
    .contact-us {
        padding-top: 25px;
    }
    .contact-left {
        padding-top: 50px;
    }
    /* ---------------------------------------------*/

    /* Feedback */
    .input-line {
        margin: 0 auto;
    }
    .enquiry {
        text-align: center;
        margin: auto;
        padding: 0 20% 0 20%;
        font-family: Calibri-Light;
    }
    .text-block {
        text-align: left;
    }
    .form-horizontal .control-label {
        padding-top: 0;
        padding-bottom: 8px;
    }
    .contact-us h1,
    .enquiry h1 {
        margin-top: 30px;
        margin-bottom: 40px;
    }
    .enquiry label {
        display: block;
        text-align: right;
        padding-right: 15px;
    }
    .enquiry input {
        display: block;
        width: 90%;
        background: #f5f5f5;
        border: 1px solid #d3d3d3;
    }
    .enquiry textarea {
        width: 90%;
        height: 100px;
        background: #f5f5f5;
        resize: none;
        border: 1px solid #d3d3d3;
    }
    .remarks {
        color: #808080;
    }
    .remarks span {
        font-size: 16px;
    }
    .remarks input {
        width: 80px;
        background: #3871a4;
        color: white;
    }
    .remarks input:hover {
        background: #2e5e8a;
        transition: 0.3s;
    }
    /* ---------------------------------------------*/
    .divider {
        width: 80%;
        border-color: #c1c1c1;
    }
    .divider-2 {
        width: 100%;
        border-color: #c1c1c1;
    }

    i {
        color: #1e6ea4;
        font-size: 18px;
    }
    .footer-img {
        display: block;
        margin: 0 auto;
        height: 80%;
    }

    #myModal {
        width: 100%;
    }
    .modal-content,
    .modal-dialog {
        border-radius: 0;
    }
    .modal-header {
        border: none;
        padding-bottom: 5px;
    }
    .modal-header h4 {
        color: navy;
        text-align: center;
        font-size: 22px;
    }
    .modal-body {
        font-size: 16px;
        text-align: center;
    }

    .error {
        color: red;
        font-size: 14px;
    }




</style>
</head>
<body>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="container-fluid">
    <div class="navbar-collapse navbar-fixed-top navigation">
        <ul class="nav navbar-nav navbar-right">
            <li class="top-menu-logo">
                <a href="#home"><img class="logo-img" src="{{asset('img/logo.png')}}"></a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#home">Home</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#about">About Us</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#products">Products</a>
            </li>
            <li class="top-menu-item">
                <a class="page-link" href="#contact">Contact</a>
            </li>
        </ul>
    </div>

    <div class="row header" id="home">
        <div class="header-content">
            <img class="logo-responsive" src="{{asset('img/logo.png')}}">
        </div>
    </div>

    <div class="row about-us" id="about">
        <h1>ABOUT US</h1>
        {!! $about->contents !!}
    </div>

    <div class="row products" id="products">
        <h1>PRODUCTS</h1>
        <?php $i=1; ?>
        @foreach($products as $product)
            @if($i%2!=0)
                <div class="row product-row">
                    <div class="col-lg-5 col-sm-12 col-md-6 product-descr">
                        <h4>{{$product->title}}</h4>
                        <p>{!!$product->description!!}</p>
                    </div>
                    <div class="col-lg-2 col-sm-3 product-div product-div-r">
                        <img src="{{asset('img/products/'.$product->image3)}}">
                    </div>
                    <div class="col-lg-2 col-sm-3 product-div product-div-r">
                        <img src="{{asset('img/products/'.$product->image2)}}">
                    </div>
                    <div class="col-lg-2 col-sm-3 product-div product-div-r">
                        <img src="{{asset('img/products/'.$product->image1)}}">
                    </div>
                </div>
            @else
                <div class="row product-row">
                    <div class="hidden-sm hidden-xs col-lg-2 col-sm-3 product-div">
                        <img src="{{asset('img/products/'.$product->image1)}}">
                    </div>
                    <div class="hidden-sm hidden-xs col-lg-2 col-sm-3 product-div">
                        <img src="{{asset('img/products/'.$product->image2)}}">
                    </div>
                    <div class="hidden-sm hidden-xs col-lg-2 col-sm-3 product-div">
                        <img src="{{asset('img/products/'.$product->image3)}}">
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12 product-descr">
                        <h4>{{$product->title}}</h4>
                        <p>{!!$product->description!!}</p>
                    </div>
                    <div class="hidden-lg hidden-md col-lg-2 col-sm-3 product-div">
                        <img src="{{asset('img/products/'.$product->image1)}}">
                    </div>
                    <div class="hidden-lg hidden-md col-lg-2 col-sm-3 product-div">
                        <img src="{{asset('img/products/'.$product->image2)}}">
                    </div>
                    <div class="hidden-lg hidden-md col-lg-2 col-sm-3 product-div">
                        <img src="{{asset('img/products/'.$product->image3)}}">
                    </div>
                </div>
            @endif
                <?php $i++; ?>
            <hr class="divider-2">
        @endforeach
    </div>

    <div class="row representative">
        <h1>WE ARE A REPRESENTATIVE OF</h1>
        @foreach($companies as $company)
        <div class="col-md-4 col-xs-12 r-logo">
            <a href="@if($company->link_active!=null) {{$company->link}} @else {{asset('company/'.$company->id)}} @endif"><img src="{{asset('img/logos/'.$company->logo)}}"></a>
        </div>
        @endforeach

    </div>

    <div class="row contact-us" id="contact">
        <h1>CONTACT US</h1>
        <div class="col-sm-offset-2 col-sm-4 contact-left">
            <h4>{{$address->company_name}}</h4>
            <p style="padding-top: 15px;">{{$address->address_1}}<br>{{$address->address_2}}</p>

            <p style="padding-top: 15px;"><i class="fa fa-phone"></i>&nbsp;{{$address->tel_1}}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$address->tel_2}}</p>

            <p style="padding-top: 15px;"><i class="fa fa-fax "></i>&nbsp;{{$address->fax}}</p>

            <p><i class="fa fa-envelope-o"></i>&nbsp;{{$address->email}}</p>
        </div>
        <div class="col-sm-5">
            <div id="map">{!!$map->url!!}</div>
        </div>
    </div>

    <hr class="divider">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 19px; height: 20px; width: 20px; margin-top: -2px; border: 1px solid #d3d3d3; border-radius: 20px;"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Successful notification</h4>
                </div>
                <div class="modal-body">
                   <p>Thank you for your enquire/feedback. We will get back to you as soon as possible.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalFail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 19px; height: 20px; width: 20px; margin-top: -2px; border: 1px solid #d3d3d3; border-radius: 20px;"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Warning</h4>
                </div>
                <div class="modal-body">
                    <p>Attachment is too large.</p>
                </div>
            </div>
        </div>
    </div>

    <div id="waiting" >
        <img src="{{asset('img/sending-msg.gif')}}">
    </div>

    <div class="enquiry">
        <h1>SEND AN ENQUIRY:</h1>
        <form class="form-horizontal" action="/send" method="post" id="enq-form" enctype="multipart/form-data">
            <input type="hidden" id="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="title">Subject<span>*</span>:</label>
                <div class="col-sm-9">
                    <input type="text" name="title" id="title">
                    <span id="titleError" class="error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name">Name<span>*</span>:</label>
                <div class="col-sm-9">
                    <input type="text" name="name" id="name">
                    <span id="nameError" class="error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="company">Company<span>*</span>:</label>
                <div class="col-sm-9 ">
                    <input type="text" name="company" id="company">
                    <span id="companyError" class="error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="email">Email<span>*</span>:</label>
                <div class="col-sm-9">
                    <input type="email" name="email" id="email">
                    <span id="emailError" class="error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="tel">Tel<span>*</span>:</label>
                <div class="col-sm-9">
                    <input type="text" name="tel" id="tel">
                    <span id="telError" class="error"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label shift" for="feedback">Enquiry/Feedback<span>*</span>:</label>
                <div class="col-sm-9 text-block">
                    <textarea class="input-area" name="feedback" id="feedback"></textarea>
                </div>
                <span id="feedbackError" class="error"></span>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="drawing">Drawing (attachment):</label>
                <div class="col-sm-9">
                    <input class="file-input" type="file" name="drawing" id="drawing">
                </div>
            </div>
            <div class="remarks">
                <p class="col-sm-offset-3 col-sm-2"><span>*</span> compulsory</p>
                <div class="col-sm-offset-5 col-sm-2">
                    <input type="submit" class="submit-button"  value="Submit" id="send-btn">
                </div>
            </div>
        </form>
    </div>

</div>
<footer>
<img class="footer-img" src="{{asset('img/footer-img.png')}}">
    <p class="created-by-logo-string"><span>Created by</span>  <a href="https://businessbuddy.sg/"><img class="managed-by" src="{{asset('img/created-by-logo.png')}}"></a></p>
</footer>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
<script>
    /*function initMap() { //Google maps 1.363291, 103.887685
        var myLatLng = {lat: parseFloat('{{$map->latitude}}'), lng: parseFloat('{{$map->longitude}}')};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Business Buddy Pte Ltd',
            icon: '{{asset('img/marker.png')}}'
        });
        google.maps.event.addListener(map, 'click', function(event) {
            console.log(event.latLng.lat() + " " + event.latLng.lng());
        });
    }*/ //----------
    $(document).ready(function(){
        $('#waiting').hide();
        $(document).on('focus', 'input', function() {
            $(this).css('border-color', '#d3d3d3');
            $('#'+$(this).attr('name')+'Error').text('');
        });
        $(document).on('focus', 'textarea', function() {
            $(this).css('border-color', '#d3d3d3');
        });
        var $options = {
            beforeSend: step,
            beforeSubmit:  check,  // pre-submit callback
            success:       gotIt,
            error: gotFail
            // post-submit callback
                    // clear all form fields after successful submit
        };

        $('#enq-form').ajaxForm($options);

        function step() {
            $('#waiting').show();
        }
    function check() {
        var $errors = 0;

        var emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var mainPattern = /^[a-zA-Z0-9\-\s]{3,}$/;
        var messagePattern = /.{3,}/;

        if(!mainPattern.test($('#title').val())) {
            $('#titleError').text('Input correct title please');
            $('#title').css('border-color', 'red');
            $errors++;
        }
        if(!mainPattern.test($('#name').val())) {
            $('#nameError').text('Input correct name please');
            $('#name').css('border-color', 'red');
            $errors++;
        }
        if(!mainPattern.test($('#company').val())) {
            $('#companyError').text('Input correct company name please');
            $('#company').css('border-color', 'red');
            $errors++;
        }
        if(!emailPattern.test($('#email').val())) {
            $('#emailError').text('Input correct email please');
            $('#email').css('border-color', 'red');
            $errors++;
        }
        if(!messagePattern.test($('#tel').val())) {
            $('#telError').text('Input correct company telephone please');
            $('#tel').css('border-color', 'red');
            $errors++;
        }
        if(!messagePattern.test($('#feedback').val())) {
            $('#feedback').css('border-color', 'red');
            $errors++;
        }

        return $errors!=0?false:true;
    }
    function gotIt() {
        $('#waiting').hide();
        $('#myModal').modal('show');
        $('#_token').val('');
        $('#title').val('');
        $('#name').val('');
        $('#company').val('');
        $('#email').val('');
        $('#tel').val('');
        $('#feedback').val('');
        $('#delivery_time').val('');
        $('#drawing').val('');
    }
    function gotFail() {
        $('#waiting').hide();
        $('#myModalFail').modal('show');
    }
    });
    $('a').click(function(){
       $(this).css('background', 'inherit');
    });
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    if(window.innerWidth < 765){
                        $('html,body').animate({
                            scrollTop: target.offset().top - 10
                        }, 1000);
                    }else {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 50
                        }, 1000);
                    }
                    return false;
                }
            }
        });
    });
</script>
<!--script async defer
        src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap">
</script-->